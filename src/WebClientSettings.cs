namespace Jig.VisualStudio.Extensions.WebClient
{
    using System;
    using System.Collections.Specialized;

    /// <summary>
    /// The web client settings.
    /// </summary>
    internal class WebClientSettings
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WebClientSettings"/> class.
        /// </summary>
        public WebClientSettings()
        {
            this.QueryString = new NameValueCollection(StringComparer.InvariantCultureIgnoreCase);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the query string.
        /// </summary>
        public NameValueCollection QueryString { get; private set; }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public Uri Url { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the output extension.
        /// </summary>
        public string OutputExtension { get; set; }

        #endregion
    }
}