﻿namespace Jig.VisualStudio.Extensions.WebClient
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Cache;
    using System.Runtime.InteropServices;
    using System.Text;

    using Jig.VisualStudio.Extensions.WebClient.Properties;

    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Designer.Interfaces;
    using Microsoft.VisualStudio.OLE.Interop;
    using Microsoft.VisualStudio.Shell;
    using Microsoft.VisualStudio.Shell.Interop;

    using IServiceProvider = Microsoft.VisualStudio.OLE.Interop.IServiceProvider;
    using Task = System.Threading.Tasks.Task;

    /// <summary>
    /// The diamond generator.
    /// </summary>
    /// <remarks>The GUID of Generator must match source.extension.vsixmanifest 
    /// <![CDATA[<Identifier Id="20160125-7105-4EE5-93C3-A3DED3CBB874">]]>
    /// </remarks>
    [ComVisible(true)]
    [Guid(JigWebClientGenerator.GuidFileGeneratorString)]
    [ProvideObject(typeof(JigWebClientGenerator))]
    [Microsoft.VisualStudio.Shell.CodeGeneratorRegistration(typeof(JigWebClientGenerator), "JigWebClientGenerator", VsContextGuidVcsProject, GeneratesDesignTimeSource = true)]
    public class JigWebClientGenerator : IVsSingleFileGenerator, IObjectWithSite
    {
        #region Constants

        /// <summary>
        /// The vs context unique identifier VCS project
        /// </summary>
        public const string VsContextGuidVcsProject = "{FAE04EC1-301F-11D3-BF4B-00C04F79EFBC}";

        /// <summary>
        /// The guid file generator string.
        /// </summary>
        public const string GuidFileGeneratorString = "20160125-7105-4EE5-93C3-A3DED3CBB874";

        #endregion

        #region Static Fields

        /// <summary>
        /// The guid file generator.
        /// </summary>
        public static readonly Guid GuidFileGenerator = new Guid(GuidFileGeneratorString);

        /// <summary>
        /// The xml example
        /// </summary>
        private static readonly string XmlExample = Environment.NewLine + "Xml Example: " + Environment.NewLine + Settings.Default.Example;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the code DOM provider.
        /// </summary>
        /// <value>
        /// The code DOM provider.
        /// </value>
        private CodeDomProvider CodeDomProvider { get; set; }

        /// <summary>
        /// Gets the code provider.
        /// </summary>
        private CodeDomProvider CodeProvider
        {
            get
            {
                if (this.CodeDomProvider == null)
                {
                    var provider =
                        (IVSMDCodeDomProvider)this.SiteServiceProvider.GetService(typeof(IVSMDCodeDomProvider).GUID);
                    if (provider != null)
                    {
                        this.CodeDomProvider = (CodeDomProvider)provider.CodeDomProvider;
                    }
                }

                return this.CodeDomProvider;
            }
        }

        /// <summary>
        /// Gets or sets the service provider.
        /// </summary>
        /// <value>
        /// The service provider.
        /// </value>
        private ServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// Gets or sets the site.
        /// </summary>
        /// <value>
        /// The site.
        /// </value>
        private object Site { get; set; }

        /// <summary>
        /// Gets or sets the settings.
        /// </summary>
        /// <value>The settings.</value>
        private WebClientSettings WebClientSettings { get; set; }

        /// <summary>
        /// Gets the site service provider.
        /// </summary>
        private ServiceProvider SiteServiceProvider
        {
            get
            {
                if (this.ServiceProvider == null)
                {
                    var oleServiceProvider = this.Site as IServiceProvider;
                    this.ServiceProvider = new ServiceProvider(oleServiceProvider);
                }

                return this.ServiceProvider;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Generated file default extension.
        /// </summary>
        /// <param name="defaultExtension">The default extension.</param>
        /// <returns>
        /// The VSConstants.
        /// </returns>
        public int DefaultExtension(out string defaultExtension)
        {
            if (this.WebClientSettings != null && !string.IsNullOrWhiteSpace(this.WebClientSettings.OutputExtension))
            {
                defaultExtension = ".Generated." + this.WebClientSettings.OutputExtension;
            }
            else
            {
                defaultExtension = ".Generated." + this.CodeProvider.FileExtension;
            }
            
            return VSConstants.S_OK;
        }

        /// <summary>
        /// The generate.
        /// </summary>
        /// <param name="inputFilePath">The input file path.</param>
        /// <param name="inputFileContents">The input file contents.</param>
        /// <param name="defaultNamespace">The default namespace.</param>
        /// <param name="outputFileContents">The output file contents.</param>
        /// <param name="output">The output.</param>
        /// <param name="generateProgress">The generate progress.</param>
        /// <returns>
        /// The <see cref="int" />.
        /// </returns>
        /// <exception cref="System.ArgumentException">The settings is null!</exception>
        public int Generate(
            string inputFilePath,
            string inputFileContents,
            string defaultNamespace,
            IntPtr[] outputFileContents,
            out uint output,
            IVsGeneratorProgress generateProgress)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(inputFileContents))
                {
                    var ex = new ArgumentException("inputFileContents");
                    Trace.TraceError(ex.Message);
                    throw ex;
                }

                var parser = new XDocumentParse(inputFileContents);
                this.WebClientSettings = parser.Parse();

                if (this.WebClientSettings == null)
                {
                    Trace.TraceError("The settings is null!");
                    Trace.TraceError(Settings.Default.Example);
                    throw new ArgumentException("The settings is null!");
                }

                var source = this.GetCodeSource(this.WebClientSettings);
                if (!string.IsNullOrWhiteSpace(source))
                {
                    byte[] bytes = Encoding.UTF8.GetBytes(source);

                    if (bytes == null)
                    {
                        outputFileContents[0] = IntPtr.Zero;
                        output = 0;
                    }
                    else
                    {
                        outputFileContents[0] = Marshal.AllocCoTaskMem(bytes.Length);
                        Marshal.Copy(bytes, 0, outputFileContents[0], bytes.Length);
                        output = (uint)bytes.Length;
                    }

                    return VSConstants.S_OK;
                }

                output = 0;

                return VSConstants.E_FAIL;
            }
            catch (Exception exception)
            {
                Trace.TraceError(exception.ToString());
                if (generateProgress != null)
                {
                    generateProgress.GeneratorError(0, 0, exception.Message, 0, 0);
                    var data = exception.Data["Data"] as string;
                    if (!string.IsNullOrWhiteSpace(data))
                    {
                        generateProgress.GeneratorError(0, 0, data, 0, 0);
                    }

                    generateProgress.GeneratorError(0, 0, XmlExample, 0, 0);
                }

                outputFileContents[0] = IntPtr.Zero;
                output = 0;
            }

            return VSConstants.E_FAIL;
        }

        /// <summary>
        /// Gets the site.
        /// </summary>
        /// <param name="registrationId">
        /// The registration unique identifier.
        /// </param>
        /// <param name="site">
        /// The site.
        /// </param>
        public void GetSite(ref Guid registrationId, out IntPtr site)
        {
            if (this.Site == null)
            {
                Marshal.ThrowExceptionForHR(VSConstants.E_NOINTERFACE);
            }

            // Query for the interface using the site object initially passed to the generator
            IntPtr punk = Marshal.GetIUnknownForObject(this.Site);
            int hr = Marshal.QueryInterface(punk, ref registrationId, out site);
            Marshal.Release(punk);
            ErrorHandler.ThrowOnFailure(hr);
        }

        /// <summary>
        /// The set site.
        /// </summary>
        /// <param name="site">
        /// The site.
        /// </param>
        public void SetSite(object site)
        {
            this.Site = site;

            this.CodeDomProvider = null;
            this.ServiceProvider = null;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get code source.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// The <see cref="Task" />.
        /// </returns>
        private string GetCodeSource(WebClientSettings settings)
        {
            using (var client = new System.Net.WebClient
            {
                QueryString = settings.QueryString,
                CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore)
            })
            {
                if (!string.IsNullOrWhiteSpace(settings.UserName) && !string.IsNullOrWhiteSpace(settings.Password))
                {
                    client.Credentials = new NetworkCredential(userName: settings.UserName, password: settings.Password);
                }

                Trace.TraceInformation("Connecting to url: " + settings.Url);

                string source = client.DownloadString(settings.Url);
                return source;
            }
        }

        #endregion
    }
}