﻿namespace UnitTestProject
{
    using System;
    using System.IO;
    using System.Net;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Jig.VisualStudio.Extensions.WebClient;

    /// <summary>
    /// The unit test.
    /// </summary>
    [TestClass]
    public partial class UnitTest
    {
        #region Public Methods and Operators

        /// <summary>
        /// The test x document parse.
        /// </summary>
        [TestMethod]
        public void TestXDocumentParse()
        {
            try
            {
                var xml = File.ReadAllText("Labels.xml");
                var parser = new XDocumentParse(xml);
                var settings = parser.Parse();
                Assert.IsNotNull(settings);

                var client = new WebClient
                                 {
                                     QueryString = settings.QueryString
                                 };

                var result = client.DownloadString(settings.Url);
                Assert.IsNotNull(result);
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }

        #endregion
    }
}