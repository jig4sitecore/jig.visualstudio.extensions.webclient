﻿namespace Jig.VisualStudio.Extensions.WebClient
{
    using System;
    using System.Diagnostics;
    using System.Xml.Linq;

    /// <summary>
    /// The x document parse.
    /// </summary>
    internal partial class XDocumentParse
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XDocumentParse" /> class.
        /// </summary>
        /// <param name="xml">The xml.</param>
        public XDocumentParse(string xml)
        {
            if (string.IsNullOrWhiteSpace(xml))
            {
                throw new ArgumentNullException("xml");
            }

            this.Xml = xml;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the xml.
        /// </summary>
        public string Xml { get; private set; }

        #endregion

        /// <summary>
        /// Parses this instance.
        /// </summary>
        /// <returns>The web settings for the web client</returns>
        /// <exception cref="System.ArgumentException">
        /// The url attribute on root node is missing!
        /// or
        /// The url attribute is not valid Uri:  + urlAttr.Value
        /// </exception>
        public WebClientSettings Parse()
        {
            WebClientSettings settings;
            try
            {
                settings = new WebClientSettings();
                var document = XDocument.Parse(this.Xml);
                var urlAttr = document.Root.Attribute(XName.Get("url"));
                if (string.IsNullOrWhiteSpace(urlAttr.Value))
                {
                    Trace.TraceError("The url attribute on root node is missing!");
                    throw new ArgumentException("The url attribute on root node is missing!");
                }

                Uri url;
                if (!Uri.TryCreate(urlAttr.Value, UriKind.Absolute, out url))
                {
                    Trace.TraceError("The url attribute is not valid Uri: " + urlAttr.Value);
                    throw new ArgumentException("The url attribute is not valid Uri: " + urlAttr.Value);
                }

                settings.Url = url;

                var nodes = document.Root.Elements();

                foreach (var node in nodes)
                {
                    settings.QueryString[node.Name.LocalName] = node.Value;
                }

                var attr = document.Root.Attribute(XName.Get("userName"));
                if (attr != null && !string.IsNullOrWhiteSpace(attr.Value))
                {
                    settings.UserName = attr.Value;
                    attr = document.Root.Attribute(XName.Get("password"));
                    if (attr != null && !string.IsNullOrWhiteSpace(attr.Value))
                    {
                        settings.Password = attr.Value;
                    }
                }

                attr = document.Root.Attribute(XName.Get("extension"));
                if (attr != null && !string.IsNullOrWhiteSpace(attr.Value))
                {
                    settings.OutputExtension = attr.Value;
                }
            }
            catch (Exception ex)
            {
                var msg = "Input:" + Environment.NewLine + this.Xml;
                ex.Data["Data"] = msg;
                Trace.TraceError(ex.Message);
                Trace.TraceError(msg);
                throw;
            }

            return settings;
        }
    }
}